module.exports = (sequelize, type) => {
    return sequelize.define('user', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: type.STRING
        },
        email: {
            type: type.STRING
        },
        created_at: {
            type: type.DATE
        }
    })
}