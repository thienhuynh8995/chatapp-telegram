module.exports = (sequelize, type) => {
    return sequelize.define('website', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        domain: {
            type: type.STRING
        },
        email: {
            type: type.STRING
        },
        phone: {
            type: type.STRING
        },
        chat_id: {
            type: type.STRING
        },
        user_id: {
            type: type.INTEGER
        },
        title_closed: {
            type: type.STRING
        },
        title_open: {
            type: type.STRING
        },
        intro_message: {
            type: type.STRING
        },
        auto_response: {
            type: type.STRING
        },
        auto_no_response: {
            type: type.STRING
        },
        placeholder_text: {
            type: type.STRING
        },
        get_customer_info_text: {
            type: type.STRING
        },
        main_color: {
            type: type.STRING
        },
        closed_chat_avatar_url: {
            type: type.STRING
        },
        facebook_link: {
            type: type.STRING
        },
        whatsapp_link: {
            type: type.STRING
        },
        start_date: {
            type: type.DATE,
            defaultValue: type.NOW
        },
        expire_date: {
            type: type.DATE,
            defaultValue: type.NOW
        },
        last_add_bill_date: {
            type: type.DATE,
            defaultValue: type.NOW
        }
    })
}