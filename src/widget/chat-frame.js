import { h, Component } from 'preact';

export default class ChatFrame extends Component {

    shouldComponentUpdate() {
        // do not re-render via diff:
        return false;
    }

    render({destinationId, host, iFrameSrc, isMobile, conf},{}) {
        conf.isMobile = isMobile;
        let encodedConf = encodeURIComponent(JSON.stringify({...conf}));
        return (
            <iframe src={iFrameSrc + '?id=' + destinationId + '&host=' + host + '&conf=' + encodedConf }
                    width='100%'
                    height={isMobile ? '100%' : '100%'}
                    frameborder='0' >
            </iframe>
        );
    }
}
