
export const defaultConfigurationVN = {
    titleClosed: 'Liên hệ',
    titleOpen: 'Liên hệ',
    introMessage: 'Xin chào, chúng tôi có thể giúp gì cho bạn?',
    placeholderText: 'Gửi tin nhắn...',
    autoResponse: 'Đang kết nối, xin vui lòng chờ trong giây lát',
    autoNoResponse: 'Hiện tại đang không có nhân viên trực, xin vui lòng liên hệ lại sau!',
    getCustomerInfoText: 'Xin vui lòng nhập thông tin của bạn để chúng tôi liên hệ!',
    alwaysUseFloatingButton: false, // Use the mobile floating button also on large screens
};
