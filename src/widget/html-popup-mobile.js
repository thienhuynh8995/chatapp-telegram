import {h, Component} from 'preact';
import {icon} from '@fortawesome/fontawesome-svg-core'
import faFreeRegular from '@fortawesome/fontawesome-free-regular'
import {
    mobileMenuBackgroundStyle,
    mobileMenuStyle,
    topMsgPopupStyle,
    middleMsgPopupStyle,
    bottomMsgPopupStyle,
    singlePopupStyle,
    mainColorPopupStyle,
    w700
} from './style'

export default class HtmlPopupMobile extends Component {

    constructor(props) {
        super(props);
    }

    render({isMobile,isOpened, chooseState, handleCancel, handleLiveChat, handleBackground, conf}) {

        console.log('html-popup', chooseState);
        
        // chooseState = this.state.chooseState !== undefined ? this.state.chooseState : chooseState;
        // console.log(this.props.chooseState);
        let menuBackgroundStyle = { ...mobileMenuBackgroundStyle };
        let menuStyle = { ...mobileMenuStyle }
        let msgArr = {
            'liveChat': {
                'title': 'Live chat',
                'handle': handleLiveChat,
                'href': '#'
            }
        };
        if (conf.facebookLink) {
            msgArr['facebook'] = {
                'title': 'Facebook',
                'handle': this.handleStopParent,
                'href': conf.facebookLink || ''
            };
        }
        if (conf.telegramLink) {
            msgArr['telegram'] = {
                'title': 'Telegram',
                'handle': this.handleStopParent,
                'href': conf.telegramLink || ''
            };
        }
        if (conf.whatsappLink) {
            msgArr['whatapp'] = {
                'title': 'Whatsapp',
                'handle': this.handleStopParent,
                'href': conf.whatsappLink || ''
            };
        }
        msgArr['cancel'] = {
            'title': 'Cancel',
            'handle': handleCancel,
            'href': '#'
        }
        let msgLength = Object.keys(msgArr).length;
        let styleArr = [];
        switch (msgLength) {
            case 4:
                styleArr = [
                    {...topMsgPopupStyle, ...w700, ...mainColorPopupStyle}, 
                    {...middleMsgPopupStyle, ...mainColorPopupStyle}, 
                    {...bottomMsgPopupStyle, ...mainColorPopupStyle}, 
                    {...singlePopupStyle, color: 'rgb(255, 132, 127)'}
                ]
                break;
            case 3:
                styleArr = [
                    {...topMsgPopupStyle, ...w700, ...mainColorPopupStyle}, 
                    {...bottomMsgPopupStyle, ...mainColorPopupStyle}, 
                    {...singlePopupStyle, color: 'rgb(255, 132, 127)'}
                ]
                break;
            default:
                styleArr = [
                    {...singlePopupStyle, ...w700, ...mainColorPopupStyle}, 
                    {...singlePopupStyle, color: 'rgb(255, 132, 127)'}
                ]
                break;
                break;
        }


        return (
            <div style={isOpened && chooseState == 'menu' ? menuBackgroundStyle: ''} onclick={handleBackground}>
                {
                    !isOpened ?
                        <div></div>
                        :
                        (chooseState == 'menu' && isMobile ? 
                            <div style={menuStyle}>
                                {
                                    Object.keys(msgArr).map((key, index) => {
                                        let msg = msgArr[key];
                                        return (
                                            <div style={index == msgLength - 2 ? 'margin-bottom: 15px;' : ''}>
                                                <a onClick={msg['handle']}  href={msg['href']} style={styleArr[index]}>{msg['title']}</a>
                                            </div>
                                        )        
                                    })
                                }
                            </div>
                            :
                            <div></div>
                        )
                }
            </div>
        );
    }

    handleStopParent = (e) => {
        e.stopPropagation();
    }
}
