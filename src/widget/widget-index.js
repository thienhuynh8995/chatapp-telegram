import { h, render } from 'preact';
import Widget from './widget';
import {defaultConfiguration} from './default-configuration';
import {defaultConfigurationVN} from './default-configuration-vn';
import axios from 'axios';
import moment from 'moment'

if (window.attachEvent) {
    window.attachEvent('onload', injectChat);
} else {
    window.addEventListener('load', injectChat, false);
}

if (document.getElementById('reload-instagrchat')) {
    document.getElementById('reload-instagrchat').onclick = function () {
        if (document.getElementById('intergramRoot')) {
            if (!window.intergramCustomizations) {
                window.intergramCustomizations = {};
            }
            window.intergramCustomizations.isChatOpen = true;
            var elem = document.getElementById("intergramRoot");
            elem.parentElement.removeChild(elem);
        }
        injectChat();
        return false;
    }
}

function injectChat() {
    let root = document.createElement('div');
    root.id = 'intergramRoot';
    document.getElementsByTagName('body')[0].appendChild(root);
    const urlRequest = window.location.hostname ;
    const host = window.location.host || 'unknown-host';
    const serverLink = document.getElementById('simpleChat-widget').src.split('/js/widget.js')[0];
    let conf = { ...defaultConfiguration, ...window.intergramCustomizations };
    conf.requestServer = serverLink;
    conf.urlRequest = urlRequest;
    conf.isFree = false;
    conf.currentUrl = window.location.href;
    const iFrameSrc = conf.requestServer + '/chat.html';


    axios.get(conf.requestServer+'/website/'+urlRequest)
        .then((response)=>{
            if (response && response.status == 200) {
                let data = response.data;
                if (data.Code == 200) {
                    window.destinationId = data.Config.chat_id;
                    console.log(data.Config);
                    Object.keys(data.Config).map((e, k)=>{
                        if (data.Config[e] == null) {
                            delete data.Config[e];
                        }
                    });
                    console.log(data.Config);
                    Object.assign(conf, conf, { ...defaultConfigurationVN, ...data.Config, ...window.intergramCustomizations });
                    console.log(conf);
                    if (data.Data.expire_date && data.Data.expire_date != null) {
                        const expireDate = moment(data.Data.expire_date).locale('vi').format('YYYY-MM-DD')
                        const today = moment().locale('vi').format('YYYY-MM-DD')
                        if (today <= expireDate) {
                            render(
                                <Widget destinationId={window.destinationId}
                                        host={host}
                                        isMobile={window.screen.width < 500}
                                        iFrameSrc={iFrameSrc}
                                        conf={conf}
                                />,
                                root
                            );

                            try {
                                const request = new XMLHttpRequest();
                                request.open('POST', conf.requestServer + '/usage-start?host=' + host);
                                request.send();
                            } catch (e) { /* Fail silently */ }
                        } else {
                            check_allow(conf);
                        }
                    } else {
                        check_allow(conf);
                    }
                }
                else {
                    alert('Tài khoản | Domain chưa được đăng ký\nVui lòng đăng kí ở https://www.simplechat.vn');
                }
            }
        })

    function check_allow(conf) {
        axios.get(conf.requestServer+'/check_allow')
            .then((response)=>{
                if (response && response.status == 200) {
                    let data = response.data;
                    if (data.Code == 200) {
                        if (data.Data.isGlobal == true) {
                            conf.isFree = true;
                            render(
                                <Widget destinationId={window.destinationId}
                                        host={host}
                                        isMobile={window.screen.width < 500}
                                        iFrameSrc={iFrameSrc}
                                        conf={conf}
                                />,
                                root
                            );

                            try {
                                const request = new XMLHttpRequest();
                                request.open('POST', conf.requestServer + '/usage-start?host=' + host);
                                request.send();
                            } catch (e) { /* Fail silently */ }
                        }
                    }
                }
            })
    }
}
